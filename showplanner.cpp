#include "showplanner.h"
#include <QtMath>
#include <QDebug>

#define DEFAULT_INTERVAL                        5000
#define DEFAULT_REPLAYOLD                       true
#define DEFAULT_INTERVALFACTORTHRESHOLD         5
#define DEFAULT_INTERVALFACTOR                  0.9

ShowPlanner::ShowPlanner(QObject *parent) : QObject(parent),
    defaultShowInterval(DEFAULT_INTERVAL),
    replayOldMedias(DEFAULT_REPLAYOLD),
    intervalFactorThreshold(DEFAULT_INTERVALFACTORTHRESHOLD),
    intervalFactor(DEFAULT_INTERVALFACTOR)
{
    showTimer = new QTimer(this);
    showTimer->setInterval(defaultShowInterval);
    connect(showTimer,&QTimer::timeout,this,&ShowPlanner::showNext);
}

void ShowPlanner::showNext(){
    bool mediaIsNew = false;
    if(!newMediaQueue.isEmpty() || replayOldMedias){
        QString mediaPath;
        if(!newMediaQueue.isEmpty()){//Show new media from queue
            mediaPath = newMediaQueue.dequeue();
            mediaIsNew = true;
        }
        else{   //replay random old media
            int randomMediaIndex = qrand() % mediaTweetMap.size();
            QList <QString> mediaPathList = mediaTweetMap.keys();
            mediaPath = mediaPathList.at(randomMediaIndex);
        }

        updateTimerInterval();

        emit mediaToDisplay(mediaTweetMap.value(mediaPath),mediaPath,mediaIsNew);
    }
    else{   //Nothing to do, no need to let the timer running
        showTimer->stop();
    }
}


void ShowPlanner::addMediaToShow(QString tweetPath, QString mediaPath){

    mediaTweetMap.insert(mediaPath,tweetPath);
    newMediaQueue.enqueue(mediaPath);

    if(!showTimer->isActive()){ //If timer not running start it
        showTimer->start(defaultShowInterval);
        showNext();
    }
    else{
        updateTimerInterval(); //If already running, adjust interval
    }
}

void ShowPlanner::updateTimerInterval(){
    //Set timer interval with reduction if more than intervalFactorThreshold new media are awaiting
    int timerInterval = defaultShowInterval;
    if(intervalFactorThreshold > 0  && newMediaQueue.size() > intervalFactorThreshold){
        int mediaSurplus = newMediaQueue.size() - intervalFactorThreshold;
        timerInterval *= qPow(intervalFactor,mediaSurplus);
    }

    showTimer->setInterval(timerInterval);
    //qDebug()<< "ShowPlanner : " << newMediaQueue << " media waiting, interval " << timerInterval << endl;
}

void ShowPlanner::loadSettings(QSettings *settingsFile){
    settingsFile->beginGroup("Slideshow");
    defaultShowInterval =       settingsFile->value("Delay",                    QVariant(DEFAULT_INTERVAL)                  ).toInt();
    replayOldMedias =           settingsFile->value("ReplayPreviousMedia",      QVariant(DEFAULT_REPLAYOLD)                 ).toBool();
    intervalFactor =            settingsFile->value("IntervalFactor",           QVariant(DEFAULT_INTERVALFACTOR)            ).toReal();
    intervalFactorThreshold =   settingsFile->value("IntervalFactorThreshold",  QVariant(DEFAULT_INTERVALFACTORTHRESHOLD)   ).toInt();
    settingsFile->endGroup();
}
