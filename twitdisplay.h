#ifndef MAINDISPLAY_H
#define MAINDISPLAY_H


#include <QWidget>
#include <QSettings>

#include "twitstreamer.h"

class TwitDisplay : public QWidget
{
    Q_OBJECT

public:
    explicit TwitDisplay(QWidget *parent = 0);

    void setSettingsFile(QSettings* settings);


public slots:
    void displayTweet(QString tweetPath, QString imagePath, bool mediaIsNew);
    void displaySplashScreen();
    virtual void show();
    void setLinkStatus(LinkStatus status);

private slots:
    void setFullScreen(bool full);

protected:
    void paintEvent(QPaintEvent * event);
    void mouseDoubleClickEvent(QMouseEvent * event);
    void keyPressEvent(QKeyEvent* event);

private:
    void buildAndEnableContextMenu();

    int textPixelSize;

    QAction* toggleFullScreenAction;
    QAction* quitAction;

    QPixmap mediaPixmap;
    QPixmap linkStatusPixMap;
    QPixmap newMediaPixMap;

    bool mediaIsNew;

    QString text;

    QSettings* settingsFile;
};

#endif // MAINDISPLAY_H
