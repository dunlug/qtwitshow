#include "mediadownloader.h"

#include <QUrl>
#include <QNetworkRequest>
#include <QMimeType>
#include <QMimeDatabase>
#include <QJsonValue>

MediaDownloader::MediaDownloader(const QString tweetPath, const QString saveDir, const QJsonObject mediaJSON, QObject *parent) :
    QObject(parent),tweetPath_(tweetPath),filePath_(saveDir),reply_(NULL)
{
    //Adding media ID to file path
    filePath_.append("/");
    filePath_.append(mediaJSON.value("id_str").toString());

    mediaURL_ = mediaJSON.value("media_url_https").toString();

    //Complete the URL by adding the prefix for the largest size available
    QJsonObject sizesJSON = mediaJSON.value("sizes").toObject();
    QString largestSize = findLargestMediaSize(sizesJSON);
    if(!largestSize.isEmpty()){
        mediaURL_.append(":");
        mediaURL_.append(largestSize);
    }

    file_ = new QFile(this);
}

QString MediaDownloader::findLargestMediaSize(QJsonObject sizesJSON){
    QString largestSize;
    int maxPixelCount = 0;
    foreach(QString sizeString, sizesJSON.keys()){
        QJsonObject currentSizeObject = sizesJSON.value(sizeString).toObject();
        int w = currentSizeObject.value("w").toInt();
        int h = currentSizeObject.value("h").toInt();
        int pixelCount = w * h;
        if(pixelCount > maxPixelCount){
            maxPixelCount = pixelCount;
            largestSize = sizeString;
        }
    }
    return largestSize;
}

void MediaDownloader::start(QNetworkAccessManager *nam){

    //Sending request
    QUrl url = QUrl(mediaURL_);
    QNetworkRequest request(url);
    reply_= nam->get(request);
    reply_->setParent(this);

    connect(reply_,&QNetworkReply::readyRead,this,&MediaDownloader::onDataReceived);
    connect(reply_,&QNetworkReply::finished,this,&MediaDownloader::onReplyFinished);
}

void MediaDownloader::abort(){
    reply_->abort();
}

void MediaDownloader::onDataReceived(){
    //Create file
    if(!file_->isOpen()){
        //Add extension to file path based on MIME type of received content
        QMimeDatabase MimeDB;
        QMimeType mimeType = MimeDB.mimeTypeForData(reply_);
        filePath_.append(".");
        filePath_.append(mimeType.preferredSuffix());

        //Create file
        file_->setFileName(filePath_);
        file_->open(QFile::WriteOnly);
    }

    //Write received data to file
    file_->write(reply_->readAll());
}

void MediaDownloader::onReplyFinished(){
    if(reply_->error() == QNetworkReply::NoError){  //If download complete
        file_->close();     //Close the file
        emit success(tweetPath_,filePath_);
    }
    else{                                           //If download failed
        file_->remove();    //Close then delete the file
        emit failed(reply_->error());
    }
}

QString MediaDownloader::getErrorString(){
    return reply_->errorString();
}

bool MediaDownloader::isRunning(){
    if(reply_)
        return reply_->isRunning();
    else
        return false;
}
