QT *= network

INCLUDEPATH += $$PWD
SOURCES += \
    $$PWD/o1.cpp \
    $$PWD/o1requestor.cpp \
    $$PWD/simplecrypt.cpp \
    $$PWD/o2settingsstore.cpp \
    $$PWD/o2replyserver.cpp

HEADERS += \
    $$PWD/o1.h \
    $$PWD/o1requestor.h \
    $$PWD/o1twitter.h \
    $$PWD/o2abstractstore.h \
    $$PWD/o2settingsstore.h \
    $$PWD/o2replyserver.h
