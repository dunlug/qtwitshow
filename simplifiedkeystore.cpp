#include "simplifiedkeystore.h"

SimplifiedKeyStore::SimplifiedKeyStore(QObject *parent):
    O2AbstractStore(parent)
{

}


QString SimplifiedKeyStore::value(const QString &key, const QString &defaultValue){
    return keyMap.value(key,defaultValue);
}


void SimplifiedKeyStore::setValue(const QString &key, const QString &value){
    keyMap.insert(key,value);
}
