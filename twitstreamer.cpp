#include "twitstreamer.h"
#include "o2globals.h"
#include "o1.h"
#include "simplifiedkeystore.h"

#include <QDesktopServices>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QTimer>


#define RECONNECT_TIMER_INTERVAL                60000
#define RECONNECT_DELAY_ON_TIMEOUT              30000
#define RECONNECT_DELAY_ON_CONNECTION_CLOSED    120000

const char O2_CONSUMER_KEY[] = "IgM98fmBpbBYpavmh0J3OLIlE";
const char O2_CONSUMER_SECRET[] = "2zZfhXZ5GDfs51wCMGObdPmXMPIX4NLSgfH07kFoazEESVprYp";

const int localPort = 8888;



TwitStreamer::TwitStreamer(QObject *parent) :
    QObject(parent), reply(NULL),saveDir(QStringLiteral("saved_tweets/")),settingsFile(NULL)
{
    o1Twitter_ = new O1Twitter(this);
    o1Twitter_->setClientId(O2_CONSUMER_KEY);
    o1Twitter_->setClientSecret(O2_CONSUMER_SECRET);
    o1Twitter_->setLocalPort(localPort);
    o1Twitter_->setStore(new SimplifiedKeyStore(this));

    nam = new QNetworkAccessManager(this);
    requestor = new O1Requestor(nam, o1Twitter_, this);

    crypt = SimpleCrypt(getHash(O2_ENCRYPTION_KEY));

    timeoutDetector = new QTimer(this);
    timeoutDetector->setSingleShot(true);
    timeoutDetector->setInterval(60000);

    reconnectTimer = new QTimer(this);
    reconnectTimer->setSingleShot(true);

    // Connect signals
    connect(o1Twitter_, SIGNAL(linkingSucceeded()),
            this, SLOT(onLinkingSucceeded()));
    connect(o1Twitter_, SIGNAL(openBrowser(QUrl)),
            this, SLOT(onOpenBrowser(QUrl)));
    connect(o1Twitter_, SIGNAL(closeBrowser()),
            this, SLOT(onCloseBrowser()));
    connect(timeoutDetector, SIGNAL(timeout()),
            this, SLOT(onTimeout()));
    connect(reconnectTimer, SIGNAL(timeout()),
            this, SLOT(startStreaming()));
}

void TwitStreamer::startAuthentication(){
    emit linkStatusUpdated(LinkDown);

    //Attempt to load crypted keys from setting file
    if(settingsFile){
        settingsFile->beginGroup("AuthKeys");
        QString clientId =    settingsFile->value("ClientID",QVariant(QString())).toString();
        QString token =       settingsFile->value("Token",QVariant(QString())).toString();
        QString tokenSecret = settingsFile->value("TokenSecret",QVariant(QString())).toString();
        settingsFile->endGroup();

        //If all fields filled, decode keys
        if(!clientId.isEmpty() && !token.isEmpty() && !tokenSecret.isEmpty()){
            o1Twitter_->setClientId(crypt.decryptToString(clientId));
            o1Twitter_->setToken(crypt.decryptToString(token));
            o1Twitter_->setTokenSecret(crypt.decryptToString(tokenSecret));
        }
    }

    qDebug() << "TwitStreamer : Starting OAuth...";
    o1Twitter_->link();
}

void TwitStreamer::onLinkingSucceeded(){
    if(settingsFile){
        //Save keys if requested
        if(settingsFile->value("SaveKeys",QVariant(false)).toBool()){
            settingsFile->beginGroup("AuthKeys");
            settingsFile->setValue("ClientID"   ,crypt.encryptToString(o1Twitter_->clientId()));
            settingsFile->setValue("Token"      ,crypt.encryptToString(o1Twitter_->token()));
            settingsFile->setValue("TokenSecret",crypt.encryptToString(o1Twitter_->tokenSecret()));
            settingsFile->endGroup();
        }

        //Loading folder parameter
        if(settingsFile->contains("OutputPath")){
            setSavePath(settingsFile->value("OutputPath").toString());
        }
        else{
            setSavePath("Tweets_Data");
        }

        //Loading tracking parameters
        settingsFile->beginGroup("Tracking");
        foreach(QString key, settingsFile->childKeys()){
            if(key == QStringLiteral("Contains")){
                foreach(QString word, settingsFile->value(key).toStringList()){
                    addTrackingCriteria(TwitStreamer::CriteriaKeyword,word);
                }
            }
            else if(key == QStringLiteral("UserID")){
                foreach(QString user, settingsFile->value(key).toStringList()){
                    addTrackingCriteria(TwitStreamer::CriteriaUser,user);
                }
            }
            else if(key == QStringLiteral("Location")){
                foreach(QString coord, settingsFile->value(key).toStringList()){
                    addTrackingCriteria(TwitStreamer::CriteriaLocation,coord);
                }
            }
        }
        settingsFile->endGroup();
    }
    if(autoStartStreaming){
        startStreaming();
    }
}

void TwitStreamer::onOpenBrowser(const QUrl &url) {
    qDebug() << "TwitStreamer : Opening browser with url" << url.toString();
    QDesktopServices::openUrl(url);
}

void TwitStreamer::onCloseBrowser() {
}


void TwitStreamer::startStreaming(){
    emit linkStatusUpdated(LinkUp);

    //Abort and delete previous reply
    if(reply != NULL){
        disconnect(reply, SIGNAL(finished()), this, SLOT(onConnectionFinished()));
        reply->abort();
        reply->deleteLater();
    }

    //Clean RX buffer
    receiveBuffer.clear();

    //Abort and delete all pending media downloads
    while(!mediaDownloadQueue.isEmpty()){
        MediaDownloader* media = mediaDownloadQueue.takeFirst();
        media->abort();
        media->deleteLater();
    }

    //Send streaming request
    QByteArray postData = O1::createQueryParams(criteriaList);
    QUrl url = QUrl("https://stream.twitter.com/1.1/statuses/filter.json");
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, O2_MIME_TYPE_XFORM);
    reply = requestor->post(request, criteriaList, postData);
    reply->setParent(this);
    connect(reply, SIGNAL(readyRead()), this, SLOT(onDataReady()));
    connect(reply, SIGNAL(finished()), this, SLOT(onConnectionFinished()));

    timeoutDetector->start();
    qDebug()<<"TwitStreamer : Streaming initiated !";
}

void TwitStreamer::onConnectionFinished(){
    qDebug()<<"Connection terminated, waiting " << RECONNECT_DELAY_ON_CONNECTION_CLOSED << "ms before attempting to resume";
    emit linkStatusUpdated(LinkWait);
    timeoutDetector->stop();
    reconnectTimer->start(RECONNECT_DELAY_ON_CONNECTION_CLOSED);
}

void TwitStreamer::onTimeout(){
    qDebug()<<"Connection timed out, waiting " <<  RECONNECT_DELAY_ON_TIMEOUT << " ms before attempting restart";
    emit linkStatusUpdated(LinkDown);
    reconnectTimer->start(RECONNECT_DELAY_ON_TIMEOUT);
}

void TwitStreamer::onDataReady(){
    static QByteArray separator = QByteArray("\r\n");

    QByteArray receivedData = reply->readAll();
    receiveBuffer.append(receivedData);

    //Process block
    int separatorPosition;
    while( (separatorPosition=receiveBuffer.indexOf(separator)) >= 0){              //If separator found...
        QByteArray tweetData = receiveBuffer.left(separatorPosition);               //... extract the complete json file ...
        receiveBuffer.remove(0,separatorPosition+separator.length());               //... and delete it from receive buffer
        if(tweetData.length() > 0 ){                                                //If the file is not empty...
            processTweet(tweetData);
        }
        else{
            qDebug()<<"TwitStreamer : Empty reply ignored";
        }
    }

    timeoutDetector->start();
}

void TwitStreamer::processTweet(QByteArray tweetData){
    QJsonParseError parsingError;
    QJsonDocument tweetDocument = QJsonDocument::fromJson(tweetData, &parsingError);

    //Attempt to parse data received
    if(parsingError.error != QJsonParseError::NoError){
        qDebug()<<"TwitStreamer : Parsing Error :"<<parsingError.errorString();
        qDebug()<<"Data :"<<QString(tweetData);
        return;
    }

    //Ensure parsed JSON is an object
    if(!tweetDocument.isObject()){
        qDebug()<<"TwitStreamer : Non-object JSON received";
        return;
    }

    //Create destination folder if required
    if(!createFolderIfNotFound(saveDir)){
        qDebug()<<"TwitStreamer : Could not create destination path, tweet not saved";
        return;
    }

    //Getting JSON object
    QJsonObject tweet = tweetDocument.object();

    //Saving JSON to file
    QString tweetID = tweet.value("id_str").toString();
    QString filePath = saveDir.path() +  QStringLiteral("/%1.tweet").arg(tweetID);
    QFile tweetFile(filePath,this);
    tweetFile.open(QIODevice::WriteOnly);
    tweetFile.write(tweetDocument.toJson());
    tweetFile.close();

    qDebug()<<"TwitStreamer : Tweet"<<tweetID<<"saved :" << tweet.value("text").toString();

    //Parsing JSON to get media infos
    QJsonObject tweetEntities = tweet.value("extended_entities").toObject();
    QJsonArray tweetMediaArray = tweetEntities.value("media").toArray();
    foreach(const QJsonValue & media, tweetMediaArray){
        QJsonObject mediaObject = media.toObject();

        //If not a photo, skip it
        if(mediaObject.value("type") != QStringLiteral("photo")) continue;

        QString mediaSavePath = saveDir.path();

        //Create new MediaDownloader object and connect it
        MediaDownloader* mediaDownloader = new MediaDownloader(filePath,mediaSavePath,mediaObject,this);
        connect(mediaDownloader,&MediaDownloader::success,this,&TwitStreamer::newMediaAvailable);
        connect(mediaDownloader,&MediaDownloader::success,this,&TwitStreamer::mediaDownloadComplete);
        connect(mediaDownloader,&MediaDownloader::failed,this,&TwitStreamer::mediaDownloadFailed);

        //Add it to media list
        mediaDownloadQueue.append(mediaDownloader);

        //If list was previously empty, start downloading this media.
        //Else, download is queued
        if(mediaDownloadQueue.size() == 1){
            startNextMediaDownload();
        }
    }
}

void TwitStreamer::startNextMediaDownload(){
    if(mediaDownloadQueue.isEmpty())return;
    foreach(MediaDownloader* downloader,mediaDownloadQueue){
        if(!downloader->isRunning()){
            downloader->start(nam);
            return;
        }
    }
}

void TwitStreamer::mediaDownloadComplete(QString tweetID, QString mediaPath){
    //Remove media from queue, mark it for deletion and start downloading next media
    Q_UNUSED(tweetID);
    MediaDownloader* completedDownloader = dynamic_cast<MediaDownloader*>(sender());
    mediaDownloadQueue.removeAll(completedDownloader);
    completedDownloader->deleteLater();
    qDebug()<<"TwitStreamer : Media downloaded"<<mediaPath;
    startNextMediaDownload();
}

void TwitStreamer::mediaDownloadFailed(){
    //Put this media at the end of the queue and qtart downloading next media
    MediaDownloader* failedDownloader = dynamic_cast<MediaDownloader*>(sender());
    mediaDownloadQueue.removeAll(failedDownloader);
    mediaDownloadQueue.append(failedDownloader);
    qDebug()<<"TwitStreamer : Media download failed"<<failedDownloader->getErrorString();
    startNextMediaDownload();
}


void TwitStreamer::addTrackingCriteria(CriteriaType type, QString param){
    O1RequestParameter criteria(QByteArray(),param.toUtf8());
    switch(type){
    case CriteriaKeyword:
        criteria.name = QByteArray("track");
        break;
    case CriteriaUser:
        criteria.name = QByteArray("follow");
        break;
    case CriteriaLocation:
        criteria.name = QByteArray("locations");
        break;
    default:
        break;
    }
    criteriaList.append(criteria);
}


TwitStreamer::CriteriaType TwitStreamer::getCriteriaType(O1RequestParameter criteria){
    if(criteria.name == QByteArray("track"))
        return CriteriaKeyword;

    else if(criteria.name == QByteArray("follow"))
        return CriteriaUser;

    else if(criteria.name == QByteArray("locations"))
        return CriteriaLocation;

    return CriteriaUndefined;
}

bool TwitStreamer::setSavePath(QString path){
    QDir newSaveDir(path);

    if(!createFolderIfNotFound(newSaveDir))
        return false;

    saveDir.swap(newSaveDir);
    return true;
}

bool TwitStreamer::createFolderIfNotFound(QDir dir){
    if(!dir.exists()){               //If destination folder does not exist...
        dir.mkpath(QString("."));    //... attempt to create it
        if(!dir.exists()){
            return false;
        }
    }
    return true;
}

void TwitStreamer::setKeys(TwitKeys keys){
    o1Twitter_->setClientId(crypt.decryptToString(keys.clientId));
    o1Twitter_->setToken(crypt.decryptToString(keys.token));
    o1Twitter_->setTokenSecret(crypt.decryptToString(keys.tokenSecret));
}

TwitKeys TwitStreamer::getKeys(){
    TwitKeys keys;
    keys.clientId       =   crypt.encryptToString(o1Twitter_->clientId());
    keys.token          =   crypt.encryptToString(o1Twitter_->token());
    keys.tokenSecret    =   crypt.encryptToString(o1Twitter_->tokenSecret());
    return keys;
}

quint64 TwitStreamer::getHash(const QString &encryptionKey) {
    return QCryptographicHash::hash(encryptionKey.toLatin1(), QCryptographicHash::Sha1).toULongLong();
}

void TwitStreamer::setSettingsFile(QSettings *newSettingsFile){
    this->settingsFile = newSettingsFile;
}

void TwitStreamer::setAutoStartStreaming(bool enabled){
    autoStartStreaming=enabled;
}
